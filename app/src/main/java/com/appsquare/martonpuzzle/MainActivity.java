package com.appsquare.martonpuzzle;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends Activity
{
	private final int countX = 3, countY = 3;
	private Random randomGenerator = new Random();
	private Timer timer = null;

	// UI
	private TextView timeLabel;
	private ImageView originalImage;
	private int originalImageRes;
	private RelativeLayout gameView;

	// Game
	private int imageWidth, imageHeight;
	private int size = 0;

	private int currentLevel = 0;
	private int elapsedTime = 0;
	private Point emptySpace;

	private class MyTimerTask extends TimerTask
	{
		@Override
		public void run()
		{
			elapsedTime++;

			MainActivity.this.runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					timeLabel.setText(String.format("%d:%02d", (int) (elapsedTime / 60), elapsedTime % 60));
				}
			});

		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);

		timeLabel = (TextView) findViewById(R.id.time);
		originalImage = (ImageView) findViewById(R.id.originalImage);
		gameView = (RelativeLayout) findViewById(R.id.gameView);

		currentLevel = 1;
	}

	public void loadLevel(int level)
	{
		gameView.setBackgroundResource(0);

		if (level == 4)
		{
			AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
			alert.setTitle(getString(R.string.gameover));
			alert.setMessage(getString(R.string.gameover_desc));
			alert.setPositiveButton("OK", null);
			alert.create().show();
		}

		elapsedTime = 0;
		timeLabel.setText("0:00");

		originalImageRes = getResources().getIdentifier("puzzle_" + level, "drawable", getPackageName());
		originalImage.setImageResource(originalImageRes);

		Bitmap originalBitmap = ((BitmapDrawable) originalImage.getDrawable()).getBitmap();

		imageWidth = originalBitmap.getWidth() / countX;
		imageHeight = originalBitmap.getHeight() / countY;

		emptySpace = new Point(countX - 1, countY - 1);
		// Resources r = getResources();
		// float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5,
		// r.getDisplayMetrics());

		int width = gameView.getWidth();
		int height = gameView.getHeight();

		size = (width < height ? width : height);
		size = size / countX;

		for (int y = 0; y < countY; y++)
		{
			for (int x = 0; x < countX; x++)
			{
				if (x == countX - 1 && y == countY - 1) break;

				int randomX = 0;
				int randomY = 0;

				boolean ok = false;

				do
				{
					randomX = randomGenerator.nextInt(countX);
					randomY = randomGenerator.nextInt(countY);

					ok = false;

					if (randomX == countX - 1 && randomY == countY - 1)
					{
						ok = true;
					}
					else
					{
						for (int i = 0; i < gameView.getChildCount(); i++)
						{
							ImageView imageView = (ImageView) gameView.getChildAt(i);

							int virtualX = ((MarginLayoutParams) imageView.getLayoutParams()).leftMargin / size;
							int virtualY = ((MarginLayoutParams) imageView.getLayoutParams()).topMargin / size;

							if (virtualX == randomX && virtualY == randomY)
							{
								ok = true;
								break;
							}
						}
					}

				} while (ok);

				Bitmap image = Bitmap.createBitmap(originalBitmap, x * imageWidth, y * imageHeight, imageWidth - 1, imageHeight - 1);

				ImageView imageView = new ImageView(this);
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(size - 1, size - 1);
				// RelativeLayout.LayoutParams params = new
				// RelativeLayout.LayoutParams(size.x - 1, size.x - 1);

				// params.leftMargin = randomX * imageWidth;
				// params.topMargin = randomY * imageHeight;

				params.leftMargin = randomX * size;
				params.topMargin = randomY * size;

				imageView.setTag(y * countX + x);
				imageView.setImageBitmap(image);
				imageView.setClickable(true);

				imageView.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(final View v)
					{
						if (v.getAnimation() != null) return;

						int x = ((MarginLayoutParams) v.getLayoutParams()).leftMargin;
						int y = ((MarginLayoutParams) v.getLayoutParams()).topMargin;

						if (isImageNearEmptySpace(x, y))
						{
							final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) v.getLayoutParams();
							params.leftMargin = emptySpace.x * size;
							params.topMargin = emptySpace.y * size;
							v.setLayoutParams(params);

							int diffX = params.leftMargin - x;
							int diffY = params.topMargin - y;

							TranslateAnimation animation = new TranslateAnimation(-diffX, 0, -diffY, 0);
							animation.setDuration(175);
							v.startAnimation(animation);

							emptySpace = new Point(x / size, y / size);

							if (isGameOver())
							{
								if (timer != null)
								{
									timer.cancel();
									timer.purge();
								}

								timer = null;

								v.clearAnimation();
								gameView.setBackgroundResource(originalImageRes);
								gameView.removeAllViews();

								AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
								alert.setTitle(getString(R.string.congrat));
								alert.setMessage(getString(R.string.levelcompleted));
								alert.setNegativeButton(getString(R.string.gameover), null);
								alert.setPositiveButton(getString(R.string.nextpicture), new DialogInterface.OnClickListener()
								{
									@Override
									public void onClick(DialogInterface dialog, int which)
									{
										currentLevel++;
										loadLevel(currentLevel);
									}
								});

								alert.create().show();
							}
						}

					}
				});

				gameView.addView(imageView, params);
			}
		}

		if (timer == null)
		{
			timer = new Timer("counter", true);
			TimerTask task = new MyTimerTask();
			timer.schedule(task, 0, 1000);
		}
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus)
	{
		super.onWindowFocusChanged(hasFocus);

		if (hasFocus)
		{
			if (size == 0) loadLevel(currentLevel);
		}
	}

	private boolean isGameOver()
	{
		boolean over = true;
		for (int i = 0; i < gameView.getChildCount(); i++)
		{
			ImageView imageView = (ImageView) gameView.getChildAt(i);

			int realX = (Integer) imageView.getTag() % countX;
			int realY = ((Integer) imageView.getTag() - realX) / countY;

			int virtualX = ((MarginLayoutParams) imageView.getLayoutParams()).leftMargin / size;
			int virtualY = ((MarginLayoutParams) imageView.getLayoutParams()).topMargin / size;

			if (realX != virtualX || realY != virtualY)
			{
				over = false;
				break;
			}
		}

		return over;
	}

	private boolean isImageNearEmptySpace(int _x, int _y)
	{
		int x = _x / size;
		int y = _y / size;

		int diffX = Math.abs(x - emptySpace.x);
		int diffY = Math.abs(y - emptySpace.y);

		return (diffX + diffY == 1);
	}

}
